# parser : créé une liste des lignes chaque ligne est une liste de Int
# EX le fichier test rend : contents = [[25, 20], [5], [2, 5], [5, 17], [11, 4], [16, 6], [20, 1]]
contents = []
a = input()
while a:
    try:
        contents.append(list(map(int, a.split())))
        a = input()
    except EOFError:
        break

# init des variables
l, h = contents[0]
n = contents[1][0]
points = contents[2:]
maxArea = 0
points.insert(0, [0, h])  # ajouter le point du debut
points.append([l, h])  # ajouter le point de la fin


# 2 eme algo en N²
def nAuCarre(points):
    pointsC = points
    rtn = 0
    for i in pointsC[:-1]:  # on laisse le point [0,0]
        yMin = h
        for j in pointsC[pointsC.index(i) + 1:]:
            rtn = max((j[0] - i[0]) * yMin, rtn)
            yMin = min(yMin, j[1])
    return rtn


# retourneun tuple ( Liste1, Liste2, Ymin)
def cutYmin(pts):
    idy = 0
    for i in range(len(pts)):
        if pts[idy][1] > pts[i][1]:
            idy = i
        elif pts[idy][1] == pts[i][1]:
            dIdyC = abs(idy - (len(pts) / 2))  # distance de l'idx par rapport au centre
            dIC = abs(i - (len(pts) / 2))  # distance de i par rapport au centre
            idy = i if dIC < dIdyC else idy
            # si la distance de i par rapport au centre et plus petite que la distance de idx alors on prend celle de i
    return pts[:idy], pts[idy:], pts[idy][1]


# Liste de points
def divideForReign(pts):
    l1, l2, ymin = cutYmin(pts)
    return max(nAuCarre(l1), nAuCarre(l2), ymin * l)


# 1er algo en N^3
def nAuCube(points):
    from itertools import combinations
    rtn = 0
    paireDePoints = [i for i in combinations(points, 2)]  # créé une liste de paires des points
    for pdp in paireDePoints:
        minArea = (pdp[1][0] - pdp[0][0]) * h
        for p in points:
            if pdp[0][0] < p[0] < pdp[1][0]:
                minArea = min((pdp[1][0] - pdp[0][0]) * p[1], minArea)
        rtn = max(minArea, rtn)
    return rtn


# Out
#maxArea = divideForReign(points)
#maxArea = nAuCarre(points)
maxArea = nAuCube(points)
print(maxArea)
